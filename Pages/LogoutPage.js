"use strict";
var module=require("../GenericLibraries/Generic Wrapper.js")
var object=require("../GenericLibraries/Utilities.js")
var obj=require("../ObjectProperties/objects.json");

exports.logoutpage = function(){
    return{
   Logout : function() {
     module.wrapper().clickfunction(object.util().getObjectLocator(obj.dashboardpage.logout_link.toString()));
     module.wrapper().clickfunction(object.util().getObjectLocator(obj.logoutpage.logout_option_yes.toString()));   
  }
 }
}