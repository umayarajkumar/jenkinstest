// spec.js
"use strict";
var input=require("../TestData/input.json");
var module=require("../GenericLibraries/Generic Wrapper.js")
var homepage=require("../Pages/HomePage.js")
var logoutpage=require("../Pages/LogoutPage.js")
var i=0;
describe('Protractor Demo App - youravon.com', function() {
    //console.log('Starting test suite "GUEST"');
beforeEach(function(){

i++;
});
  beforeAll(function() {
      homepage.page().geturl(input.url);
      
  });

  it('should login successfully', function() {
console.log(i);
     homepage.page().Login(input.username,input.password);
     module.wrapper().expectTitle(input.dashboard);

  });
it('should logout successfully', function() {
console.log(i);
     logoutpage.logoutpage().Logout();
     module.wrapper().expectTitle(input.logout);
  });

});