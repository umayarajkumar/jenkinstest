//var object=require("../protractor-conf/GenericLibraries/Excel Utility.js")
var DEFAULT_TIMEOUT_INTERVAL=350000;
//var broname= object.excelutil().browsername();
exports.config = {
  framework: 'jasmine2',
  seleniumAddress: 'http://localhost:4444/wd/hub',
  specs:['./Specs/spec.js'],
  
  multiCapabilities: [
    {browserName: "chrome"},
   
  ],
//JSON.stringify(object.excelutil().browsername())
  // baseUrl: 'http://localhost:4444?spec=Protractor+Demo+App+-+youravon.com+should+login+successfully',
jasmineNodeOpts: {
        defaultTimeoutInterval: DEFAULT_TIMEOUT_INTERVAL,

  },
  onPrepare: function() {
   // var jasmineReporters = require('jasmine-reporters');
    var Jasmine2HtmlReporter = require('protractor-jasmine2-html-reporter');
  let SpecReporter = require('jasmine-spec-reporter').SpecReporter;

    return browser.getProcessedConfig().then(function(config) {
       
        var browserName = config.capabilities.browserName;

       // var junitReporter = new jasmineReporters.JUnitXmlReporter({
           // consolidateAll: true,
           // savePath: './Reports',
          //  cleanDestination:false,
          
           // filePrefix:'xmlresults',
            
            
        //});
        //jasmine.getEnv().addReporter(junitReporter);



         jasmine.getEnv().addReporter(
          new Jasmine2HtmlReporter({
            savePath: './Reports/Screenshots/'+'Screenshot' +'-'+ new Date().getDate()+'-'+ ((new Date().getMonth())+1)+'-'+ new Date().getFullYear(),
            fixedScreenshotName: true,
            
            cleanDestination:false,

           
          })
          
        ); 

        
        jasmine.getEnv().addReporter(
          new SpecReporter({
            displayStacktrace: false,
            displayFailuresSummary: false,
            displayPendingSummary:true,
            displaySuccessfulSpec:true,
            displayFailedSpec:true,
            displayPendingSpec:true,
            colors:{
              success:'green',
              failure:'red',
              pending:'yellow',
            },
            
          })
          );
            
var AllureReporter = require('jasmine-allure-reporter');
   // jasmine.getEnv().addReporter(new AllureReporter({
    //  resultsDir: 'allure-results'
  //  }));
     jasmine.getEnv().addReporter(new AllureReporter());
    jasmine.getEnv().afterEach(function(done){
      browser.takeScreenshot().then(function (png) {
        allure.createAttachment('Screenshot', function () {
          return new Buffer(png, 'base64')
        }, 'image/png')();
        done();
      })
    });

           
        });
        
  },

      

}
