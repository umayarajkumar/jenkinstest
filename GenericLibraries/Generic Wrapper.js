 exports.wrapper = function(){
   return {
        /* Get Url Function */

        getUrl : function(url){
            browser.get(url);
        },

        /*  Maximize the browser */

        maximize : function(){
            browser.driver.manage().window().maximize();
        },

        /*  sendKeys function */
         
         sendkeysfunction : function(locator, text){
             
             element(locator).sendKeys(text);
         },
         
         /* Clear function */

         clearfunction : function(locator){
             element(locator).clear();
         },

         /* Click function  */

         clickfunction : function (locator){
             element(locator).click();
         },

         /* getText function */

         gettextfunction : function(locator){
            element(locator).getText();
         },

         /* isDisplayed function */

         isDisplayedfunction : function(locator){
             element(locator).isDisplayed();
         },

         /* isSelected function */

         isSelectedfunction : function(locator){
            element(locator).isSelected();
         },

         /* isEnabled function */

         isEnabledfunction : function(locator){
            element(locator).isEnabled();  
         },

         /* dropdown function for <ul>, <li> Tags by using index */

         dropdownlist : function(locator, index){
             element.all(locator).get(index).click();
         },

         /* Expect Title function */

         expectTitle : function(text){
            expect(browser.getTitle()).toEqual(text);
         }
   };
 }