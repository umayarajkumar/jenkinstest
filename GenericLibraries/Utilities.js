"use strict";
var obj=require("../ObjectProperties/objects.json");
var homepage=require("../Pages/HomePage.js")

 exports.util = function(){
return{
	getObjectLocator: function(locator){
    	// Read value using the logical name as Key
		
		//console.log('********************************');
		//	console.log(locator);
		//console.log(typeof(String.parse(str)));
		//var locator = obj.model_first;
        // console.log(locator)
		var locatorType = locator.split(":")[0];
		 // console.log(locatorType)
		var locatorValue = locator.split(":")[1];
		 //  console.log(locatorValue)
		// Return a instance of By class based on type of locator
		if (locatorType =='binding')
			return by.binding(locatorValue);
		else if (locatorType =='id')
			return by.id(locatorValue);
        else if (locatorType=='model')
			return by.model(locatorValue);
        else if (locatorType=='extraBinding')
			return by.extraBinding(locatorValue); 
		else if (locatorType=='css')
			return by.css(locatorValue);
		else if (locatorType=='xpath')
			return by.xpath(locatorValue);
		else if (locatorType=='cssContainingText')
			return by.cssContainingText(locatorValue, text);
		else
 			throw new Exception("Locator type '" + locatorType + "' not defined!!");
		}
	}
}